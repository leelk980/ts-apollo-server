import { ApolloServer } from 'apollo-server';
import { schema } from './schema';
import { createContext } from './context';
// import './generated/nexus';

const port = 3000;

const server = new ApolloServer({
  schema,
  context: ({ req }) => ({
    req,
    prisma: createContext().prisma,
  }),
  playground: true,
  introspection: true,
});

server.listen({ port }, () => {
  console.log(`Server is running on ${port}`);
});
