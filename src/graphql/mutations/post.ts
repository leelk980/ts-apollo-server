import { extendType } from 'nexus';

export const postMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.crud.createOnePost();
    t.crud.deleteOnePost();
    t.crud.updateOnePost();
  },
});
